package com.demo.web2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.web2.entity.User;
import com.demo.web2.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserService userService;
		
		@PostMapping("/create")
		public User createUser(@RequestBody User user) {
			return userService.createUser(user);
		}
	}
