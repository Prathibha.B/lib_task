package com.demo.web2.service;

import com.demo.web2.entity.User;

public interface UserService {
	
	User createUser(User user);

}
