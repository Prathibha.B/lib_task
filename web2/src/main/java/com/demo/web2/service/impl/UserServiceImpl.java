package com.demo.web2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.web2.entity.User;
import com.demo.web2.repository.UserRepo;
import com.demo.web2.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepo userRepo;
	
	@Override
	public User createUser(User user) {
		return userRepo.save(user);
	}
}
