package com.demo.web2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.web2.entity.User;

public interface UserRepo extends JpaRepository<User,Long> {

}
