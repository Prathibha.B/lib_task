package com.demo.web1.client;


import org.springframework.stereotype.Service;

import com.demo.lib.client.UserClient;
import com.demo.lib.dto.User;

@Service
public class UserOperationsClient {

	UserClient client = new UserClient();
	
	public User createuser(User user) {
		
		return client.createUser(user);
	}
}
